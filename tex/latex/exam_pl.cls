% set up exam questions in Polish
% usage example:
%
% \documentclass[answers]{exam_pl} or \documentclass[noanswers]{exam_pl}
% \newcommand{\answerspace}{\fillwithlines{1in}}
%
% \begin{document}
% 	\examdata{Kolokwium zaliczeniowe}{2023--2024Z}{Informatyka}{niestacjonarne}{I}{1}
%
% 	\begin{questions}
% 		\question[2]
% 		Imię Einsteina:
% 		\begin{choices}
% 			\choice Adolf,
% 			\CorrectChoice Albert,
% 			\choice Tomasz,
% 			\choice Łukasz.
% 		\end{choices}
%
% 	\question
% 	Wyjaśnij krótko:
% 	\begin{parts}
% 		\part[4]
% 		jak wyznacza się Gwiazdę Polarną.
% 		\answerspace
%
% 		\part[4]
% 		jak powstaje zorza polarna.
% 		\answerspace
% 	\end{parts}
% 	\end{questions}
%
% 	\gtable
% \end{document}

\ProvidesClass{exam_pl}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{exam}}
\ProcessOptions\relax
\LoadClass[addpoints, 12pt]{exam}
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[polish]{babel}
\RequirePackage{amsmath}
\RequirePackage[per-mode=symbol, exponent-product=\cdot]{siunitx}
\RequirePackage{physics}
\RequirePackage{mhchem}
\RequirePackage[colorlinks]{hyperref}

% global math display style
\everymath{\displaystyle}

% page numbers
\footer{}{\thepage}{}

% exam data
\newcommand{\examdata}[6]{
	\begin{center}
		\textbf{#1} \\
		#2 \\
		#3, studia~#4~#5~stopnia, semestr~\num{#6} \\
		\vspace{0.5cm}
		\makebox[\textwidth]{Imię i nazwisko:\enspace\hrulefill}
	\end{center}
}

% questions and points
\newcommand{\qname}{Zadanie}
\newcommand{\pointsuffix}{~p.}
\pointname{\pointsuffix}
\qformat{\textbf{\qname~\thequestion.}~(\totalpoints\pointsuffix)\hfill}

% solutions
\renewcommand{\solutiontitle}{\noindent\textbf{Rozwiązanie:}\enspace}

% grading table
\newcommand{\gtable}{
	\hrulefill
	\begin{center}
		\hqword{\qname}
		\hpword{Max}
		\hsword{Wynik}
		\htword{Suma}
		\gradetable[h]
	\end{center}
}
