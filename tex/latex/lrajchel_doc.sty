% avoid "\lll" problem with the "amssymb" package
\let\lll\undefined

%============================================================
% AMS packages
%============================================================
% load AMS math packages
\usepackage{amsmath, amssymb}

% mean absolute/signed error
\DeclareMathOperator{\MAE}{MAE}
\DeclareMathOperator{\MSE}{MSE}
%============================================================

% scripts in math mode
\newcommand{\scr}[1]{\mbox{\scriptsize{#1}}}

% text alignment in tabel
\newcommand{\tp}[2]{\multicolumn{1}{#1}{#2}}

% big quantifier with variables below it
\newcommand{\nmathlarger}[1]{\mathlarger{\mathlarger{\mathlarger{#1}}}}
\newcommand{\bigforall}[2]{\nmathlarger{\mathop{\forall}}_{#1}{#2}}

% creation operators i and j daggered, without a dot
\newcommand{\cri}{\hat{\imath}^\dagger}
\newcommand{\crj}{\hat{\jmath}^\dagger}

% annihililation operators i and j without a dot
\newcommand{\ani}{\hat{\imath}}
\newcommand{\anj}{\hat{\jmath}}

% creation operator
\newcommand{\crt}[1]{\ensuremath{\hat{a}_#1^\dagger}}

% creation operator - short form
\newcommand{\crs}[1]{\ensuremath{\hat{#1}^\dagger}}

% annihilation operator
\newcommand{\ann}[1]{\ensuremath{\hat{a}_#1}}

% creation operator in oscillator notation (a^t)
\newcommand{\cra}{\ensuremath{\hat{a}^\dagger}}

% annihilation operator in oscillator notation (a)
\newcommand{\ana}{\ensuremath{\hat{a}}}

% excited wavefunction, e.g. Psi_i^a
\newcommand{\ew}[2]{\ensuremath{\Psi_{#1}^{#2}}}

% excited wavefunction in a ket, e.g. |Psi_i^a>
\newcommand{\ewk}[2]{\ensuremath{\Ket{\ew{#1}{#2}}}}

% excitation operator
\newcommand{\exc}[2]{\ensuremath{\hat{E}_#1^#2}}

% antisymmetrisation operator
\newcommand{\asop}{\ensuremath{\hat{\mathscr{A}}}}

% anticommutator
\newcommand{\acom}[2]{\ensuremath{\left[ #1; #2 \right]_+}}

% note in table etc.
\newcommand{\tn}[1]{$^{\mbox{\scriptsize{#1}}}$}

% vacuum state: o, without any bra or ket
\newcommand{\vacs}{\ensuremath{\mathrm{o}}}

% vacuum state: |o>
\newcommand{\vac}{\ensuremath{\ket{\vacs}}}

% conjugated vacuum state: <o|
\newcommand{\vacd}{\ensuremath{\bra{\vacs}}}

% Fermi vacuum: |0>
\newcommand{\fvac}{\ensuremath{\ket{0}}}

% conjugated Fermi vacuum: <0|
\newcommand{\fvacd}{\ensuremath{\bra{0}}}

% vector symbol with a hat over it, e.g. meaning the spherical angles of the vector
\newcommand{\vh}[1]{\ensuremath{\hat{\mathbf{#1}}}}

% similarity-transformed hamiltonian
\newcommand{\sth}{\ensuremath{\hat{\widetilde{H}}}}

% similarity-transformed hamiltonian matrix
\newcommand{\shm}{\ensuremath{\widetilde{\mathbb{H}}}}

% mean value: <A>
\newcommand{\mv}[1]{\langle #1 \rangle}

% begin mathematical environment
\newcommand{\beq}{\begin{equation}}

% end mathematical environment
\newcommand{\eeq}{\end{equation}}

% begin non-numbered mathematical environment
\newcommand{\beqn}{\begin{equation*}}

% end non-numbered mathematical environment
\newcommand{\eeqn}{\end{equation*}}

% begin centered environment
\newcommand{\bec}{\begin{center}}

% end centered environment
\newcommand{\eec}{\end{center}}

% begin #1 multicols environment
\newcommand{\bem}[1]{\begin{multicols}{#1}}

% end multicols environment
\newcommand{\eem}{\end{multicols}}

% operator of the square of total angular momentum
\newcommand{\am}{\hat{\jmath}^2}

% operator of the q-th component of angular momentum
\newcommand{\amq}[1]{\hat{\jmath}_#1}

% spherical harmonic: Ylm(theta; phi)
\newcommand{\sh}[2]{Y_{#1}^{#2}(\theta; \phi)}

% theta part of sferical harmonic: Thetalm(theta)
\newcommand{\tsh}[2]{\Theta_#1^#2(\theta)}

% k-th correction to the wavefunction psi_n, not enclosed in ket
\newcommand{\fpn}[2]{\psi_#1^{(#2)}}

% k-th correction to the wavefunction psi_n, enclosed in ket
\newcommand{\fp}[2]{\Ket{\psi_#1^{(#2)}}}

% k-th correction to the energy E_n
\newcommand{\ep}[2]{E_#1^{(#2)}}

% ceiling function
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}

% floor function
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}

% electron configuration of a given element
\newcommand{\con}[2]{\mbox{#1: } #2}

% spin up (1/2)
\newcommand{\su}{\uparrow}

% spin down (-1/2)
\newcommand{\sd}{\downarrow}

% spin up and down (1/2,-1/2)
\newcommand{\sud}{\uparrow \downarrow}

% alternative mark for a vector
\newcommand{\vd}[1]{\boldsymbol{\mathfrak{#1}}}

% expression 1 / (4 pi epsilon_0)
\newcommand{\pep}{\frac{e^2}{4 \pi \epsilon_0}}

% written E, used for electronic energy
\newcommand{\ewr}{\mathcal{E}}

% trial wavefunction
\newcommand{\twf}{\widetilde{\Psi}}

% i-th trial spinorbital (with tilde over it)
\newcommand{\tsp}[1]{\widetilde{\phi}_#1}

% i-th trial spinorbital (with tilde over it) with j-th coordinate
\newcommand{\tspc}[2]{\tsp{#1}(\mathbf{q}_#2)}

% epsilon with tilde
\newcommand{\tep}{\widetilde{\epsilon}}

% wide tilde
\newcommand{\wt}{\widetilde}

% Wigner j-3 symbol
\newcommand{\jthree}[6]{\left( \begin{array}{ccc} #1 & #2 & #3 \\ #4 & #5 & #6	\end{array} \right)}

% atomic unit of energy (hartree)
\newcommand{\eau}{\ensuremath{\mathscr{E}_\mathrm{h}}}

% atomic unit of length (reduced Bohr radius)
\newcommand{\lau}{\ensuremath{\wt{a}_0}}

% atomic unit of time
\newcommand{\tmau}{\ensuremath{\mathscr{T}}}

% transposed matrix symbol, A^T
\newcommand{\tm}{\ensuremath{\mathrm{T}}}

% below are some commands for use with the picture environment - in order to draw good pictures one has to set \unitlength to some very small value, e.g. 0.008 cm

\newcounter{xpos}
\newcounter{ypos}

% draw the empty orbital; the orbital horizontal line is printed at the point (x,y)
\newcommand{\orb}[2]{
	\thicklines
	\put(#1,#2){\line(1,0){100}}
	\thinlines}

% draw the spin-alfa electron
\newcommand{\spna}{\vector(0,1){60}}

% draw the spin-beta electron
\newcommand{\spnb}{\vector(0,-1){60}}

% draw the orbital with spin-alfa electron; the orbital horizontal line is printed at the point (x,y)
\newcommand{\orba}[2]{
	\orb{#1}{#2}
	\setcounter{xpos}{#1}
	\setcounter{ypos}{#2}
	\addtocounter{xpos}{50}
	\addtocounter{ypos}{-30}
	\put(\thexpos,\theypos){\spa}}

% draw the orbital with spin-beta electron; the orbital horizontal line is printed at the point (x,y)
\newcommand{\orbb}[2]{
	\orb{#1}{#2}
	\setcounter{xpos}{#1}
	\setcounter{ypos}{#2}
	\addtocounter{xpos}{50}
	\addtocounter{ypos}{30}
	\put(\thexpos,\theypos){\spb}}

% draw the orbital with spin-alfa and spin-beta electrons (doubly-occupied orbital); the orbital horizontal line is printed at the point (x,y)
\newcommand{\orbab}[2]{
	\orb{#1}{#2}
	\setcounter{xpos}{#1}
	\setcounter{ypos}{#2}
	\addtocounter{xpos}{40}
	\addtocounter{ypos}{-30}
	\put(\thexpos,\theypos){\spa}
	\addtocounter{xpos}{20}
	\addtocounter{ypos}{60}
	\put(\thexpos,\theypos){\spb}}
